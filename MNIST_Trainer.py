from tqdm.notebook import tqdm
import torch
from torch import nn
import numpy as np
import matplotlib.pyplot as plt

use_cuda = torch.cuda.is_available()

def train(epoch, model, train_loader, optimizer):
    """Trainer for then MNIST data set. Highly influenced by the authors work.

    Args:
        epoch (int): Number of epochs.
        model (model): The NeuralODE which should be trained.
        train_loader (data): The Data
        optimizer (Optimizer): The optimzer for backpropagation.

    Returns:
        array: The loss for each iteration
    """    
    num_items = 0
    train_losses = []

    model.train()
    criterion = nn.CrossEntropyLoss()
    print(f"Training Epoch {epoch}...")
    for batch_idx, (data, target) in tqdm(enumerate(train_loader), total=len(train_loader)):
        if use_cuda:
            data = data.cuda()
            target = target.cuda()
        optimizer.zero_grad()
        output = model(data)
        loss = criterion(output, target) 
        loss.backward()
        optimizer.step()

        train_losses += [loss.item()]
        num_items += data.shape[0]
    print('Train loss: {:.5f}'.format(np.mean(train_losses)))
    return train_losses

def test(model, test_loader):
    """Testing the model meaning how good the accuracy is. Highly influenced by the authors work.

    Args:
        model (model): The trained model.
        test_loader (test_loader): The data to test.
    """    
    accuracy = 0.0
    num_items = 0

    model.eval()
    
    criterion = nn.CrossEntropyLoss()
    print(f"Testing...")
    with torch.no_grad():
        for batch_idx, (data, target) in tqdm(enumerate(test_loader),  total=len(test_loader)):
            if use_cuda:
                data = data.cuda()
                target = target.cuda()
            output = model(data)
            accuracy += torch.sum(torch.argmax(output, dim=1) == target).item()
            num_items += data.shape[0]
    accuracy = accuracy * 100 / num_items
    print("Test Accuracy: {:.3f}%".format(accuracy))

def test_print(model, data):
    """Printing test samples for the model.

    Args:
        model (model): The trained model.
        data (data): Test data.
    """    
    model.eval()
    criterion = nn.CrossEntropyLoss()
    print(f"Testing...")
    with torch.no_grad():
        for batch_idx, (data, target) in data:
            if use_cuda:
                data = data.cuda()
                target = target.cuda()

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns
sns.color_palette("bright")
import matplotlib.cm as cm
from torch import Tensor

dim = 100
dim_quiver = 20

def to_np(x):
    return x.detach().cpu().numpy()

def plot_trajectories(fig, obs=None, times=None, trajs=None, save=None):
    """Plot of the observations and the learned trajectory.
    """
    ax = fig.add_subplot(3,3,1)
    if obs is not None:
        if times is None:
            times = [None] * len(obs)
        for o, t in zip(obs, times):
            o, t = to_np(o), to_np(t)
            for b_i in range(o.shape[1]):
                ax.scatter(o[:, b_i, 0], o[:, b_i, 1], c=t[:, b_i, 0], cmap=cm.viridis)

    if trajs is not None: 
        for z in trajs:
            z = to_np(z)
            ax.plot(z[:, 0, 0], z[:, 0, 1], lw=1.5)
            ax.set_xlabel("x")
            ax.set_ylabel("y")  
            ax.set_title("Trajectories")
        if save is not None:
            extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
            plt.savefig(save, bbox_inches=extent.expanded(1.1, 1.2))
            
    #plt.show()
    
def plot_dynamics(trajs, obs, fig, save=None):
    """Plot of the dynamics in time.
    """
    ax = fig.add_subplot(3,3,2)
    times = np.linspace(0,len(trajs[0]),len(trajs[0]))

    for o in obs:
        o = to_np(o)
        for b_i in range(o.shape[1]):
            ax.scatter(times, o[:, b_i, 0], s=1, c='m')
            ax.scatter(times, o[:, b_i, 1], s=1, c='c')
            ax.set_xlabel("Time")
            ax.set_ylabel("y")  
            ax.set_title("Dynamics in Time")
    for z in trajs:
        z = to_np(z)
        ax.plot(times, z[:, 0, 0], lw=1.5, color='m')
        ax.plot(times, z[:, 0, 1], lw=1.5, color='c')
        if save is not None:
            extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
            plt.savefig(save, bbox_inches=extent.expanded(1.1, 1.2))    
    #plt.show()
    
def plot_phase_portrait_2D(fig, X, func, times, color, x_min=-10, x_max=10, y_min=-10, y_max=+10, save=None):
    """Plot of the phase portrait with coloerd vectors.
    """
    axes = fig.add_subplot(3,3,5)
    
    x_mesh = np.linspace(x_min, x_max, dim_quiver)
    y_mesh = np.linspace(y_min, y_max, dim_quiver)
    X1 , Y1  = np.meshgrid(x_mesh, y_mesh) # create a grid
    DX1, DY1  = func(Tensor(np.stack([X1, Y1], -1).reshape(dim_quiver * dim_quiver, 2)),times).cpu().detach().numpy().T

    M = (np.hypot(DX1, DY1))               # norm growth rate 
    M[ M == 0] = 1.                        # avoid zero division errors 
    DX1 /= M                               # normalize each arrows
    DY1 /= M

    axes = plt.gca()
    axes.set_xlim([x_min,x_max])
    axes.set_ylim([y_min,y_max])
    plt.quiver(X1, Y1, DX1, DY1, M, pivot='mid')

    for z in X:
        z = to_np(z)
        x = z[:,0,0]
        y = z[:,0,1]
        plt.plot(x, y, color=color)
        plt.xlabel("x")
        plt.ylabel("y")  
        plt.title("Phaseportrait")
        if save is not None:
            extent = axes.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
            plt.savefig(save, bbox_inches=extent.expanded(1.1, 1.2))
    #plt.show()

    
def plot_phase_2D(trajs, fig, save=None):
    """Plot of the phase portrait automatically zoomed.
    """
    ax2 = fig.add_subplot(3,3,3)
    times = np.linspace(0,len(trajs[0]),len(trajs[0]))
    for z in trajs:
        z = to_np(z)
        #print(len(times))
        #print(z.shape)

        ax2.plot(z[:, 0, 0], z[:, 0, 1], color="blue")
        ax2.set_xlabel("x")
        ax2.set_ylabel("y")  
        ax2.set_title("Phase space")
        ax2.grid()
        
        if save is not None:
            extent = ax2.get_window_extent().transformed(fig.dpi_scale_trans.inverted())    
            plt.savefig(save, bbox_inches=extent.expanded(1.1, 1.2))
    #plt.show()

        
def plot_stream(fig, func, times, x_min=-10, x_max=10, y_min=-10, y_max=+10, save=None):
    """Plot of the stream plot.
    """
    axes2 = fig.add_subplot(3,3,4)

    x_mesh = np.linspace(x_min, x_max, dim)
    y_mesh = np.linspace(y_min, y_max, dim)
    X1 , Y1  = np.meshgrid(x_mesh, y_mesh) # create a grid
    dydt = func(Tensor(np.stack([X1, Y1], -1).reshape(dim * dim, 2)),times).cpu().detach().numpy()

    s = np.sqrt(dydt[:, 0]**2 + dydt[:, 1]**2)
    c = s.reshape(dim,dim)
    mag = s.reshape(-1, 1)
    dydt = (dydt / mag)
    dydt = dydt.reshape(dim, dim, 2)

    axes2.streamplot(X1, Y1, dydt[:, :, 0], dydt[:, :, 1], color=c, cmap=plt.cm.jet)
    axes2.set_xlim(x_min, x_max)
    axes2.set_ylim(y_min, y_max)
    axes2.set_xlabel("x")
    axes2.set_ylabel("y")  
    axes2.set_title("Streamplot")
    if save is not None:
        extent = axes2.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
        plt.savefig(save, bbox_inches=extent.expanded(1.1, 1.2))

def plot_train_error(fig, losses,save=None):
    """Plot of the error curve.
    """
    axes = fig.add_subplot(3,3,6)
    history = pd.DataFrame({"loss": losses})
    history["cum_data"] = history.index * 32
    history["smooth_loss"] = history.loss.ewm(halflife=10).mean()
    axes.set_xlabel("Iteration")
    axes.set_ylabel("Error")  
    axes.set_title("Training Error")
    axes.plot(history["cum_data"], history["smooth_loss"])
    if save is not None:
        extent = axes.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
        plt.savefig(save, bbox_inches=extent.expanded(1.1, 1.2))
                               
def plotting_fct(save):
    plt.savefig(save)
    plt.show()

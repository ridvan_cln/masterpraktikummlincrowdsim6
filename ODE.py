import torch
from torch import Tensor
from torch import nn
from torch.nn  import functional as F 
from torch.autograd import Variable

use_cuda = torch.cuda.is_available()

"""Highly influenced by the authors work."""

def norm(dim):
    """Batch normalization
    """    
    return nn.BatchNorm2d(dim)

def conv3x3(in_feats, out_feats, stride=1):
    """Convolution
    """
    return nn.Conv2d(in_feats, out_feats, kernel_size=3, stride=stride, padding=1, bias=False)

def add_time(in_tensor, t):
    """Adding the time to images.
    """
    bs, c, w, h = in_tensor.shape
    return torch.cat((in_tensor, t.expand(bs, 1, w, h)), dim=1)


class ODEF(nn.Module):
    def forward_with_grad(self, z, t, grad_outputs):
        """Compute f and a df/dz, a df/dp, a df/dt"""
        batch_size = z.shape[0]

        out = self.forward(z, t)

        a = grad_outputs
        adfdz, adfdt, *adfdp = torch.autograd.grad(
            (out,), (z, t) + tuple(self.parameters()), grad_outputs=(a),
            allow_unused=True, retain_graph=True
        )
        # grad method automatically sums gradients for batch items, we have to expand them back 
        if adfdp is not None:
            adfdp = torch.cat([p_grad.flatten() for p_grad in adfdp]).unsqueeze(0)
            adfdp = adfdp.expand(batch_size, -1) / batch_size
        if adfdt is not None:
            adfdt = adfdt.expand(batch_size, 1) / batch_size
        return out, adfdz, adfdt, adfdp

    def flatten_parameters(self):
        p_shapes = []
        flat_parameters = []
        for p in self.parameters():
            p_shapes.append(p.size())
            flat_parameters.append(p.flatten())
        return torch.cat(flat_parameters)
    
class LinearODEF(ODEF):
    """Abstract class for Linear ODEs
    """
    def __init__(self, W):
        super(LinearODEF, self).__init__()
        self.lin = nn.Linear(2, 2, bias=False)
        self.lin.weight = nn.Parameter(W)

    def forward(self, x, t):
        return self.lin(x)
    
class SpiralFunctionExample(LinearODEF):
    """This is the ODE as a Network for the Spiralfunction for Task3.
    """    
    def __init__(self):
        super(SpiralFunctionExample, self).__init__(Tensor([[.1, .1], [-.25, 0]]))
        
class RandomLinearODEF(LinearODEF):
    """This is the learned Network for the Spiralfunction for Task3.
    """
    def __init__(self):
        super(RandomLinearODEF, self).__init__(torch.randn(2, 2)/4.)
        
class ConvODEF(ODEF):
    """This is the Network for transforming images to an ODE 
    serving as an input for the NeuralODE.
    """    
    def __init__(self, dim):
        super(ConvODEF, self).__init__()
        self.conv1 = conv3x3(dim + 1, dim)
        self.norm1 = norm(dim)
        self.conv2 = conv3x3(dim + 1, dim)
        self.norm2 = norm(dim)

    def forward(self, x, t):
        xt = add_time(x, t)
        h = self.norm1(torch.relu(self.conv1(xt)))
        ht = add_time(h, t)
        dxdt = self.norm2(torch.relu(self.conv2(ht)))
        return dxdt

class ContinuousNeuralMNISTClassifier(nn.Module):
    """This is the Network for transforming output of the NeuralODE 
    to an classifier.
    """ 
    def __init__(self, ode):
        super(ContinuousNeuralMNISTClassifier, self).__init__()
        self.downsampling = nn.Sequential(
            nn.Conv2d(1, 64, 3, 1),
            norm(64),
            nn.ReLU(inplace=True),
            nn.Conv2d(64, 64, 4, 2, 1),
            norm(64),
            nn.ReLU(inplace=True),
            nn.Conv2d(64, 64, 4, 2, 1),
        )
        self.feature = ode
        self.norm = norm(64)
        self.avg_pool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Linear(64, 10)

    def forward(self, x):
        x = self.downsampling(x)
        x = self.feature(x)
        x = self.norm(x)
        x = self.avg_pool(x)
        shape = torch.prod(torch.tensor(x.shape[1:])).item()
        x = x.view(-1, shape)
        out = self.fc(x)
        return out

class HopfODE(ODEF):
    """This is the Network for the HOPF System ODE.
    """ 
    def __init__(self, A, B):
        super(HopfODE, self).__init__()
        self.A = nn.Linear(2, 2, bias=False)
        self.A.weight = nn.Parameter(A)
        self.B = nn.Linear(2, 2, bias=False)
        self.B.weight = nn.Parameter(B)

    def forward(self, x, t):
        xTx = torch.sum(x*x, dim=1)
        dxdt = self.A(x) - self.B(x) * xTx
        return dxdt
        


class NNODEF(ODEF):
    """This is the learned Network for the HOPF System.
    """
    def __init__(self, in_dim, hid_dim, time_invariant=False):
        super(NNODEF, self).__init__()
        self.time_invariant = time_invariant

        if time_invariant:
            self.lin1 = nn.Linear(in_dim, hid_dim)
        else:
            self.lin1 = nn.Linear(in_dim+1, hid_dim)
        self.lin2 = nn.Linear(hid_dim, hid_dim)
        self.lin4 = nn.Linear(32, 16)
        self.lin5 = nn.Linear(16, 10)
        self.lin6 = nn.Linear(10, 8)
        self.lin7 = nn.Linear(8, 6)
        self.lin8 = nn.Linear(6, 4)
        self.lin3 = nn.Linear(4, in_dim)
        self.elu = nn.ELU(inplace=True)

    def forward(self, x, t):
        if not self.time_invariant:
            x = torch.cat((x, t), dim=-1)

        h = self.elu(self.lin1(x))
        h = self.elu(self.lin2(h))
        h = self.elu(self.lin4(h))
        h = self.elu(self.lin5(h))
        h = self.elu(self.lin6(h))
        h = self.elu(self.lin7(h))
        h = self.elu(self.lin8(h))
        out = self.lin3(h)
        return out


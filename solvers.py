import math

def ode_solve_euler(z0, t0, t1, f):
    """
    Simplest Euler ODE initial value solver
    """
    h_max = 0.05
    n_steps = math.ceil((abs(t1 - t0)/h_max).max().item())

    h = (t1 - t0)/n_steps
    t = t0
    z = z0

    for _ in range(n_steps):
        z = z + h * f(z, t)
        t = t + h
    return z

def ode_solve_rk(z0, t0, t1, f):
    h_max = 0.05
    n_steps = math.ceil((abs(t1 - t0)/h_max).max().item())

    h = (t1 - t0)/n_steps
    t = t0
    z = z0
    
    for _ in range(n_steps):
        k1 = h * f(z, t)
        k2 = h * f(z + 0.5 * k1, t + 0.5 * h)
        k3 = h * f(z + 0.5 * k2, t + 0.5 * h)
        k4 = h * f(z + k3, t + h)
    
        t = t + h
        z = z + (k1 + k2 + k2 + k3 + k3 + k4) / 6
    
    return z


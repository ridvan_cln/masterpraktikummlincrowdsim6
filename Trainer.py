import numpy as np
import matplotlib.pyplot as plt
from IPython.display import clear_output

import torch
from torch import Tensor
from torch import nn
from torch.nn  import functional as F 
from torch.autograd import Variable
from plotODE import plot_trajectories, plot_dynamics, plot_phase_portrait_2D, plot_phase_2D, plot_stream, plot_train_error, plotting_fct

use_cuda = torch.cuda.is_available()

class Trainer:
    def __init__(self, n_steps, n_points=500, t_max=6.29*30):
        self.n_steps = n_steps
        self.n_points = n_points
        self.t_max = t_max

    def train(self, ode_true, ode_trained, name, plot_freq=10):
        # Create data
        z0 = Variable(torch.Tensor([[0.01, 0.01]]))

        index_np = np.arange(0, self.n_points, 1, dtype=np.int)
        index_np = np.hstack([index_np[:, None]])
        times_np = np.linspace(0, self.t_max, num=self.n_points)
        times_np = np.hstack([times_np[:, None]])

        times = torch.from_numpy(times_np[:, :, None]).to(z0)
        obs = ode_true(z0, times, return_whole_sequence=True).detach()
        #obs = obs + torch.randn_like(obs) * 0.01

        # Get trajectory of random timespan 
        min_delta_time = 1.0
        max_delta_time = 5.0
        max_points_num = 32
        def create_batch():
            t0 = np.random.uniform(0, self.t_max - max_delta_time)
            t1 = t0 + np.random.uniform(min_delta_time, max_delta_time)

            idx = sorted(np.random.permutation(index_np[(times_np > t0) & (times_np < t1)])[:max_points_num])

            obs_ = obs[idx]
            ts_ = times[idx]
            return obs_, ts_

        # Train Neural ODE
        optimizer = torch.optim.Adam(ode_trained.parameters(), lr=0.01)
        train_losses = []
        for i in range(self.n_steps):
            obs_, ts_ = create_batch()
            #print(obs_[0])
            z_ = ode_trained(obs_[0], ts_, return_whole_sequence=True)
            loss = F.mse_loss(z_, obs_.detach())

            optimizer.zero_grad()
            loss.backward(retain_graph=True)
            train_losses += [loss.item()]
            optimizer.step()

            if i % plot_freq == 0:
                z_p = ode_trained(z0, times, return_whole_sequence=True)
                fig = plt.figure(figsize=(16,16))
                plot_trajectories(fig = fig,obs=[obs], times=[times], trajs=[z_p], save=f"{name}/Trajectories/traj{i}.png" )
                plot_dynamics(trajs=[z_p],obs=[obs], fig=fig, save=f"{name}/Dynamics/dyn{i}.png")
                plot_phase_2D(trajs=[z_p], fig=fig, save=f"{name}/Phase2D/phase2D{i}.png")
                plot_stream(fig, func=ode_trained, times=ts_, x_min=-1, x_max=1, y_min=-1, y_max=+1, save=f"{name}/Streamplot/stream{i}.png")
                plot_phase_portrait_2D(fig, X=[z_p], func=ode_trained,times=ts_, color='b', x_min=-1, x_max=1, y_min=-1, y_max=+1, save=f"{name}/PhasePortrait/phaseport{i}.png")
                plot_train_error(fig, train_losses,save=f"{name}/Error/error{i}.png")
                #plt.show()
                plotting_fct(save=f"{name}/plots/allplots{i}.png")
                clear_output(wait=True)